const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

const tableName = 'jamieeason';

exports.handler = async (event) => {
    try {
        console.log("Retrieving vide details...");
        let lastEvaluatedKey = event['queryStringParameters'] && event['queryStringParameters']['key'];

        let queryParams = {TableName: tableName, ProjectionExpression: "startTime, endTime, srcVideo, workflowStatus, errorMessage, hlsUrl, thumbNailUrl"};
        if (lastEvaluatedKey) {
            queryParams = {...queryParams, ExclusiveStartKey: {guid: lastEvaluatedKey}};
        }
        queryParams = {...queryParams, Limit:80};

        let data = await docClient.scan(queryParams).promise();
        console.log("Retrieving params", JSON.stringify(queryParams));

        if (!data.LastEvaluatedKey) {
        data = {...data, LastEvaluatedKey: { guid: null }}
        }
        console.log("Video details retrieved successfully");
        
        return getSuccessResponse(data);
    } catch (e) {
        console.log("Error:", e);
        return getFailureResponse('Something wrong');
    }
};

function getSuccessResponse(data) {
    return {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*"
        },
        "statusCode": 200,
        "body": JSON.stringify(data)
    };
};

function getFailureResponse(data) {
    return {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*"
        },
        "statusCode": 500,
        "body": JSON.stringify(data)
    };
};